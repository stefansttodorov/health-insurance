package insurance.gateway;

import insurance.model.TreatmentCostsReply;
import insurance.model.TreatmentCostsRequest;
import insurance.gateway.serializer.TreatmentSerializer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.HashMap;
import java.util.Map;

public class ProducerChainedGateway {
    // Declare Client Recieving QueueName
    private String clientRecievingQueueName;

    // Declare Gateways
    private MessageReceiver messageReceiver;
    private MessageSender messageSender;

    // Declare HashMap
    private Map<String, TreatmentCostsRequest> map;

    // Declare Serializer
    private TreatmentSerializer treatmentSerializer;

    public ProducerChainedGateway() {
        // Initialize Client Recieving Queue
        clientRecievingQueueName = "client-receive-queue-" + System.currentTimeMillis();

        // Initialize Serializer
        treatmentSerializer = new TreatmentSerializer();

        // Initialize HashMap
        this.map = new HashMap<>();

        // Initialize Gateways
        this.messageSender = new MessageSender("broker-receive-client-queue", clientRecievingQueueName);
        this.messageReceiver = new MessageReceiver(clientRecievingQueueName);

        // Set Message Listener
        this.messageReceiver.setMessageListener(message -> {
            try {
                // Get JSON String from Message
                String messageJson = ((TextMessage) message).getText();
                // Deserialize Message
                TreatmentCostsReply treatmentCostsReply = treatmentSerializer.treatmentReplyFromJson(messageJson);
                // Find Loan Request
                TreatmentCostsRequest treatmentCostsRequest = this.map.get(message.getJMSCorrelationID());
                // Return all to Loan Controller
                onLoanReplyArrived(treatmentCostsRequest, treatmentCostsReply);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    public void applyForLoan(TreatmentCostsRequest treatmentCostsRequest) throws JMSException {
        // Deserialize and create a message to be sent
        Message messageToSend = this.messageSender.createTextMessage(treatmentSerializer.treatmentRequestToJson(treatmentCostsRequest));
        // Send message
        this.messageSender.sendMessage(messageToSend);
        // Get MessageID after sending it and store it in Map along with TreatmentCostsRequest
        this.map.put(messageToSend.getJMSMessageID(), treatmentCostsRequest);
    }

    public void onLoanReplyArrived(TreatmentCostsRequest loanRequest, TreatmentCostsReply loanReply){}
}
