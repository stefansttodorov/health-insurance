package broker.gateway;

import org.glassfish.jersey.client.ClientConfig;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class TransportRestClient {
    // Save the transport service URL
    private final static String transportServiceEndpoint = "http://localhost:8080/transport/rest/";

    // Declare the webTarget used for connection to the service
    private WebTarget webTarget;

    // Declare the constructor and initialize the webTarget
    public TransportRestClient() {
        URI baseUri = UriBuilder.fromUri(transportServiceEndpoint).build();
        this.webTarget = ClientBuilder.newClient(new ClientConfig()).target(baseUri);
    }

    // Make a call to the transport service endpoint
    public double getTransportPricePerKilometer() {
        // Save the response
        Response response = this.webTarget.path("price").request(MediaType.TEXT_PLAIN).get();
        // Parse the response on return
        return Double.parseDouble(response.readEntity(String.class));
    }
}