package insurance.gateway;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class MessageReceiver {

    private Properties properties;
    private Connection connection;
    private Session session;
    private Destination receiveDestination;
    private MessageConsumer consumer;

    private static final String JMS_ADDRESS = "tcp://localhost:61616";
    private static String JMS_RECEIVER_QUEUE;


    public MessageReceiver(String messageQueue) {
        this.JMS_RECEIVER_QUEUE = messageQueue;

        try {
            // Define properties
            properties = new Properties();
            properties.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
            properties.setProperty(Context.PROVIDER_URL, JMS_ADDRESS);
            this.properties.put((String.format("queue.%s",JMS_RECEIVER_QUEUE)), JMS_RECEIVER_QUEUE);

            // Set Context
            Context jndiContext = new InitialContext(properties);
            ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");
            this.connection = connectionFactory.createConnection();
            this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Connect to the receiver destination
            this.receiveDestination = (Destination) jndiContext.lookup(JMS_RECEIVER_QUEUE);
            this.consumer = session.createConsumer(receiveDestination);

            // Start receiving messages
            this.connection.start();

        } catch(NamingException | JMSException e) {
            e.printStackTrace();
        }
    }

    // Set Message listener for the Gateway
    public void setMessageListener(MessageListener messageListener){
        try {
            this.consumer.setMessageListener(messageListener);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
