package broker.gateway;

import broker.model.HospitalCostsReply;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Aggregator {

    // Mapping aggregationID to received and expected replies
    private Map<Integer, Integer> mapAggregationIDToExpectedReplies;
    private Map<Integer, ArrayList<HospitalCostsReply>> mapAggregationIDToReceivedReplies;

    // Initialize the maps in the constructor.
    public Aggregator() {
        this.mapAggregationIDToExpectedReplies = new HashMap<>();
        this.mapAggregationIDToReceivedReplies = new HashMap<>();
    }

    // Populate the maps
    public void mapAggregates(Integer aggregationId, Integer numberOfExpectedReplies) {
        this.mapAggregationIDToExpectedReplies.put(aggregationId, numberOfExpectedReplies);
        this.mapAggregationIDToReceivedReplies.put(aggregationId, new ArrayList<>());
    }

    // Add Hospital Reply to map and check if all replies are received
    public void onNewHospitalReply(HospitalCostsReply hospitalCostsReply, Integer aggregationId) {
        this.mapAggregationIDToReceivedReplies.get(aggregationId).add(hospitalCostsReply);
        calculateCheapestReplyIfAllReceived(aggregationId);
    }

    // Check if all replies are recieved and send back the lowest one.
    private void calculateCheapestReplyIfAllReceived(Integer aggregationId) {
        ArrayList<HospitalCostsReply> hospitalCostsReplies = this.mapAggregationIDToReceivedReplies.get(aggregationId);
        if (hospitalCostsReplies.size() == this.mapAggregationIDToExpectedReplies.get(aggregationId)) {
            double minPrice = Integer.MAX_VALUE;
            HospitalCostsReply bestHospitalCostsReply = null;
            for(HospitalCostsReply reply : hospitalCostsReplies) {
                if (minPrice > reply.getPrice()) {
                    minPrice = reply.getPrice();
                    bestHospitalCostsReply = reply;
                }
            }
            onAllHospitalCostsRepliesReceived(bestHospitalCostsReply, aggregationId);
        }
    }

    // Call back that handles the communication of a hospital reply and aggregationID
    public void onAllHospitalCostsRepliesReceived(HospitalCostsReply hospitalCostsReply, Integer aggregationId) {}
}