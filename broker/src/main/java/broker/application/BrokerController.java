package broker.application;

import broker.gateway.ScatterGather;
import broker.gateway.TransportRestClient;
import broker.gateway.hospital.ChainedGateway;
import broker.model.HospitalCostsReply;
import broker.model.HospitalCostsRequest;
import broker.model.TreatmentCostsReply;
import broker.model.TreatmentCostsRequest;
import javafx.application.Platform;
import javafx.scene.control.ListView;

import javax.jms.JMSException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BrokerController {
    // Declare Chained Gateways
    private ChainedGateway hospitalChainedGateway;
    private broker.gateway.insuarance.ChainedGateway insuaranceChainedGateway;

    // Declare ListView
    public ListView<ListViewLine> lvRequestReply;

    // Declare mapping for listview
    private Map<HospitalCostsRequest, TreatmentCostsRequest> map;

    // Declaring Scatter Gather
    private ScatterGather scatterGather;

    // Declare the transport service adapter
    private TransportRestClient transportRestClient;

    public BrokerController() throws JMSException {
        // Initialize HashMap
        this.map = new HashMap<>();

        // Initialize the transport service adapter
        this.transportRestClient = new TransportRestClient();

        // Initialize Client Gateway
        this.insuaranceChainedGateway = new broker.gateway.insuarance.ChainedGateway("broker-receive-client-queue", "client-receive-queue") {
            public void onClientMessageReply(TreatmentCostsRequest treatmentCostsRequest)  {
                try {
                    // Create a Line with the new LoanRequest
                    ListViewLine lvl = new ListViewLine(treatmentCostsRequest);

                    // Add new Line to ListView
                    addListViewLineToLv(lvl);

                    // Convert TreatmentCostRequest to HospitalCostRequest
                    HospitalCostsRequest hospitalCostsRequest = new HospitalCostsRequest(
                            treatmentCostsRequest.getSsn(),
                            treatmentCostsRequest.getTreatmentCode(),
                            treatmentCostsRequest.getAge()
                    );

                    // Call Bank Gateway to process loan
                    scatterGather.requestCosts(hospitalCostsRequest);

                    // Add CorrelationID and Line to map
                    map.put(hospitalCostsRequest, treatmentCostsRequest);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        };

        // Initialize Bank ChainedGateway
        this.scatterGather = new ScatterGather("broker-receive-hospital-queue") {
            public void onHospitalCostsReplyReceived(HospitalCostsRequest hospitalCostsRequest, HospitalCostsReply hospitalCostsReply) {
                try {
                    // Retrieve line using CorrelationID
                    TreatmentCostsRequest treatmentCostsRequest = map.get(hospitalCostsRequest);
                    double transportCosts = calculateTransportPrice(treatmentCostsRequest.getTransportDistance());

                    // Convert from BankInterestReply to LoanReply
                    TreatmentCostsReply treatmentCostsReply = new TreatmentCostsReply(
                            hospitalCostsReply.getPrice(),
                            transportCosts,
                            hospitalCostsReply.getHospitalName()
                    );

                    // Display changes
                    ListViewLine listViewLine = getLvlForLoanRequest(treatmentCostsRequest);
                    listViewLine.setTreatmentReply(treatmentCostsReply);
                    addListViewLineToLv(listViewLine);

                    // Send Message to Client
                    insuaranceChainedGateway.sendTreatmentReplyMessage(treatmentCostsRequest, treatmentCostsReply);

                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    // Helper method to display the requests on the form
    private void addListViewLineToLv(ListViewLine lvl) {
        Platform.runLater(() -> {
            Iterator<ListViewLine> iterator = lvRequestReply.getItems().iterator();
            while (iterator.hasNext()) {
                ListViewLine temp = iterator.next();
                if (temp.getTreatmentRequest().equals(lvl.getTreatmentRequest())) {
                    iterator.remove();
                    break;
                }
            }
            lvRequestReply.getItems().add(lvl);
        });
    }

    // Helper method to retrieve the lines from the list view
    private ListViewLine getLvlForLoanRequest(TreatmentCostsRequest req) {
        for(ListViewLine lvl : this.lvRequestReply.getItems()) {
            if(req.equals(lvl.getTreatmentRequest())) {
                return lvl;
            }
        }
        return null;
    }

    // Wrapper method which calls the transport service adapter
    private double calculateTransportPrice(Integer transportDistance) {
        if (transportDistance > 0) {
            return this.transportRestClient.getTransportPricePerKilometer() * transportDistance;
        }
        return 0;
    }
}