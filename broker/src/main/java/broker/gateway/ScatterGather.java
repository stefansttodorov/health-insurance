package broker.gateway;

import broker.gateway.hospital.ChainedGateway;
import broker.model.HospitalCostsReply;
import broker.model.HospitalCostsRequest;

import javax.jms.JMSException;
import java.util.HashMap;
import java.util.Map;

public class ScatterGather {

    private ChainedGateway hospitalGateway;

    // Declare the RecipientList and the Aggregator
    private RecipientList hospitalRecipientList;
    private Aggregator hospitalReplyAggregator;

    // Integer Aggregation ID generator
    private int aggregationIdGenerator;

    // Declare mapping for aggregationID and BankInterestRequest
    private Map<Integer, HospitalCostsRequest> aggregationIdToHospitalCostsRequestMap;

    // Declare the constructor
    public ScatterGather(String consumerQueueName) throws JMSException {
        // Initialize aggregationIdGenerator
        this.aggregationIdGenerator = 0;

        // Initialize mapping
        this.aggregationIdToHospitalCostsRequestMap = new HashMap<>();

        // Initialize BrokerHospitalClientGateway
        this.hospitalGateway = new ChainedGateway(consumerQueueName) {
            // Implement the callback from the gateway
            public void onHospitalMessageReply(
                    HospitalCostsRequest hospitalCostsRequest,
                    HospitalCostsReply hospitalCostsReply,
                    Integer aggregationId
            ) {
                if (!aggregationIdToHospitalCostsRequestMap.containsKey(aggregationId)) {
                    aggregationIdToHospitalCostsRequestMap.put(aggregationId, hospitalCostsRequest);
                }
                hospitalReplyAggregator.onNewHospitalReply(hospitalCostsReply, aggregationId);
            }
        };
        // Initialize HospitalReplyAggregator
        this.hospitalReplyAggregator = new Aggregator() {
            public void onAllHospitalCostsRepliesReceived(
                    HospitalCostsReply hospitalCostsReply, Integer aggregationId) {
                onHospitalCostsReplyReceived(
                        aggregationIdToHospitalCostsRequestMap.get(aggregationId),
                        hospitalCostsReply
                );
            }
        };
        // Initialize HospitalRecipientList
        this.hospitalRecipientList = new RecipientList(this.hospitalGateway);
    }

    // Sends a request to the hospital through the recipient list
    public void requestCosts(HospitalCostsRequest hospitalCostsRequest) throws JMSException {
        // Send HospitalCostsRequest and store the number of hospitals to which the request was sent
        int sentCount = this.hospitalRecipientList.sendHospitalCostsRequest(
                hospitalCostsRequest, this.aggregationIdGenerator);

        // Map the sent out requests to the aggregate ID
        this.hospitalReplyAggregator.mapAggregates(this.aggregationIdGenerator, sentCount);

        // Increment the generator
        this.aggregationIdGenerator++;
    }

   // Callback for when a hospital costs reply is received
    public void onHospitalCostsReplyReceived(
            HospitalCostsRequest hospitalCostsRequest,
            HospitalCostsReply hospitalCostsReply) {}
}