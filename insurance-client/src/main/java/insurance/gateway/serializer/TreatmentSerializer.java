package insurance.gateway.serializer;

import com.google.gson.Gson;
import insurance.model.TreatmentCostsReply;
import insurance.model.TreatmentCostsRequest;

public class TreatmentSerializer {
    Gson gson;

    public TreatmentSerializer() {
        this.gson = new Gson();
    }

    // Treatment Cost Request TO JSON
    public String treatmentRequestToJson(TreatmentCostsRequest approvalRequest){
        return gson.toJson(approvalRequest);
    }

    // Treatment Cost Request FROM JSON
    public TreatmentCostsRequest treatmentRequestFromJson(String replyString){
        return gson.fromJson(replyString, TreatmentCostsRequest.class);
    }

    // Treatment Cost Reply TO JSON
    public String treatmentReplyToJson(TreatmentCostsReply approvalReply){
        return gson.toJson(approvalReply);
    }

    // Treatment Cost Reply FROM JSON
    public TreatmentCostsReply treatmentReplyFromJson(String replyString){
        return gson.fromJson(replyString, TreatmentCostsReply.class);
    }

}
