package broker.gateway.hospital;

import broker.gateway.message.Receiver;
import broker.gateway.message.Sender;
import broker.gateway.serializer.HospitalSerializer;
import broker.model.HospitalCostsReply;
import broker.model.HospitalCostsRequest;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.HashMap;
import java.util.Map;

public class ChainedGateway {
    // Declare Correlation ID Mapping
    private Map<String, HospitalCostsRequest> correlationIDMapping;

    // Declare Brokers
    private Receiver bankBrokerReceiver;
    private Sender bankBrokerSender;

    // Declare Serializers
    private HospitalSerializer hospitalSerializer;

    public ChainedGateway(String receiveQueue) {
        // Initialize Mapping
        correlationIDMapping = new HashMap<>();

        // Initialize Serializer
        this.hospitalSerializer = new HospitalSerializer();

        // Initialize Bank Brokers
        this.bankBrokerReceiver = new Receiver(receiveQueue);
        this.bankBrokerSender = new Sender();

        // Set Hospital Message Listener
        this.bankBrokerReceiver.setMessageListener(message -> {
            try {
                // Convert Message to String
                String messageJson = ((TextMessage) message).getText();
                // Deserialize Message
                HospitalCostsReply hospitalCostsReply = hospitalSerializer.hospitalReplyFromJson(messageJson);
                // Get initial request
                HospitalCostsRequest hospitalCostsRequest = correlationIDMapping.get(message.getJMSCorrelationID());
                // Send to Controller
                onHospitalMessageReply(hospitalCostsRequest, hospitalCostsReply,  message.getIntProperty("aggregationID"));
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    public void requestCosts(HospitalCostsRequest hospitalCostsRequest,  Integer aggregationId, String hospitalQueueName) throws JMSException {
        // Deserialize and create a message to be sent
        Message messageToSend = bankBrokerSender.createTextMessage(hospitalSerializer.hospitalRequestToJson(hospitalCostsRequest));
        // set aggregationId int property in the message
        messageToSend.setIntProperty("aggregationID", aggregationId);
        // Send message
        bankBrokerSender.sendMessage(messageToSend, hospitalQueueName);
        // Save the Correlation ID
        correlationIDMapping.put(messageToSend.getJMSMessageID(), hospitalCostsRequest);
    }

    // Receiving a message from the bank
    public void onHospitalMessageReply(HospitalCostsRequest hospitalCostsRequest, HospitalCostsReply hospitalCostsReply, Integer aggregationId){}

}
