package broker.gateway.insuarance;

import broker.gateway.message.Receiver;
import broker.gateway.message.Sender;
import broker.gateway.serializer.TreatmentSerializer;
import broker.model.TreatmentCostsReply;
import broker.model.TreatmentCostsRequest;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.HashMap;
import java.util.Map;

public class ChainedGateway {
    // Declare Mapping
    private Map<TreatmentCostsRequest, String> correlationIDMapping;

    // Declare Return Mapping
    private Map<TreatmentCostsRequest, Destination> returnAddressMapping;

    // Declare Brokers
    private Receiver insuaranceBrokerReceiver;
    private Sender insuaranceBrokerSender;

    // Declare Serializers
    private TreatmentSerializer treatmentSerializer;

    // Declare Message Queue Names
    private String receiverQueueName;
    private String senderQueueName;

    public ChainedGateway(String receiveQueue, String senderQueue) {
        // Initialize Mappings
        correlationIDMapping = new HashMap<>();
        returnAddressMapping = new HashMap<>();

        // Initialize Queue Names
        this.receiverQueueName = receiveQueue;
        this.senderQueueName = senderQueue;

        // Initialize Serializers
        this.treatmentSerializer = new TreatmentSerializer();

        // Initialize Insurance Brokers
        this.insuaranceBrokerReceiver = new Receiver(receiveQueue);
        this.insuaranceBrokerSender = new Sender();

        // Set Client Message Listener
        this.insuaranceBrokerReceiver.setMessageListener(message -> {
            try {
                // Get JSON String from Message
                String messageJson = ((TextMessage) message).getText();
                // Deserialize Message
                TreatmentCostsRequest messageReceived = treatmentSerializer.treatmentRequestFromJson(messageJson);
                // Return all to Loan Controller
                onClientMessageReply(messageReceived);
                // Save to mappings
                correlationIDMapping.put(messageReceived, message.getJMSMessageID());
                returnAddressMapping.put(messageReceived, message.getJMSReplyTo());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendTreatmentReplyMessage(TreatmentCostsRequest treatmentCostsRequest, TreatmentCostsReply treatmentCostsReply) throws JMSException {
        // Deserialize and create a message to be sent
        Message messageToSend = insuaranceBrokerSender.createTextMessage(treatmentSerializer.treatmentReplyToJson(treatmentCostsReply));
        // Set CorrelationID to Message
        messageToSend.setJMSCorrelationID(correlationIDMapping.get(treatmentCostsRequest));
        // Send message and provide the destination queue
        insuaranceBrokerSender.sendMessage(messageToSend, returnAddressMapping.get(treatmentCostsRequest));
    }

    // Receiving a message from the client
    public void onClientMessageReply(TreatmentCostsRequest treatmentCostsRequest) {}
}
