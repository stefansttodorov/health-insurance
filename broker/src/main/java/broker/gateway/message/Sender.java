package broker.gateway.message;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class Sender {
    private Connection connection;
    private Session session;

    private MessageProducer messageProducer;
    private Properties properties;
    private Destination destinationToSendTo;
    private String jmsAddress = "tcp://localhost:61616";

    public Sender(String destinationQueue){
        try {

            // Set and Apply Properties
            properties = new Properties();
            properties.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
            properties.setProperty(Context.PROVIDER_URL, jmsAddress);
            this.properties.put(("queue." + destinationQueue), destinationQueue);

            // Create Session
            Context jndiContext = new InitialContext(properties);
            ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");
            this.connection = connectionFactory.createConnection();
            this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Connect to the sender destination
            this.destinationToSendTo = (Destination) jndiContext.lookup(destinationQueue);
            this.messageProducer = session.createProducer(destinationToSendTo);
        } catch (NamingException | JMSException e){
            e.printStackTrace();
        }
    }

    public Sender() {
        try {
            // Set and Apply Properties
            Properties props = new Properties();
            props.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
            props.setProperty(Context.PROVIDER_URL, jmsAddress);

            // Create Session
            Context jndiContext = new InitialContext(props);
            ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext
                    .lookup("ConnectionFactory");
            this.connection = connectionFactory.createConnection();
            this.session = this.connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Create an empty producer
            this.messageProducer = this.session.createProducer(null);

        } catch (JMSException | NamingException e) {
            e.printStackTrace();
        }
    }

    // Convert the sting message to a JMS Message
    public Message createTextMessage(String messageBody) throws JMSException {
        return this.session.createTextMessage(messageBody);
    }

    // Send Message
    public void sendMessage(Message message, Destination destination) throws JMSException {
        messageProducer.send(destination, message);
    }

    // Send Message which is for a particular queue
    public void sendMessage(Message message, String queueName) throws JMSException {
        this.messageProducer.send(this.session.createQueue(queueName), message);
    }
}
