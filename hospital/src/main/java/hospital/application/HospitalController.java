package hospital.application;

import hospital.gateway.ChainedGateway;
import hospital.model.Address;
import hospital.model.HospitalCostsReply;
import hospital.model.HospitalCostsRequest;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import javax.jms.JMSException;
import java.net.URL;
import java.util.Iterator;
import java.util.ResourceBundle;

class HospitalController implements Initializable {

    @FXML
    private Label lbHospital;
    @FXML
    private Label lbAddress;
    @FXML
    private TextField tfPrice;
    @FXML
    private ListView<HospitalListLine> lvRequestReply;
    @FXML
    private Button btnSendReply;

    private final String hospitalName;
    private final Address address;
    private final String hospitalListeningQueue;

    // Declare Chained Gateway
    private ChainedGateway chainedGateway;

    // Declare the Hospital Controller
    public HospitalController(String hospitalName, Address address, String hospitalRequestQueue) {
        this.address = address;
        this.hospitalName = hospitalName;
        this.hospitalListeningQueue = hospitalRequestQueue;
    }

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources)  {
        String fullAddress = this.address.getStreet() + " " + this.address.getNumber() + ", " + this.address.getCity();
        this.lbAddress.setText(fullAddress );
        this.lbHospital.setText(this.hospitalName);

        // Initialize Chained Gateway
        chainedGateway = new ChainedGateway(hospitalListeningQueue, "broker-receive-hospital-queue"){
            public void onBrokerMessageReceived(HospitalCostsRequest hospitalCostsRequest) {
                // Put Hospital Request in ListView
                HospitalListLine hll = new HospitalListLine(hospitalCostsRequest, null);
                addListViewLineToLv(hll);
            }
        };

        // Send the hospital reply on button click
        btnSendReply.setOnAction(event -> {
            try {
                sendHospitalReply();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    public void sendHospitalReply() throws JMSException {
        HospitalListLine listLine = this.lvRequestReply.getSelectionModel().getSelectedItem();
        if (listLine != null) {
            double price = Double.parseDouble(tfPrice.getText());
            HospitalCostsReply reply = new HospitalCostsReply(price, this.hospitalName, this.address);
            listLine.setReply(reply);
            lvRequestReply.refresh();

            // Reply to hospital request
            chainedGateway.processCosts(listLine.getRequest(), reply);

            // Update Reply on UI
            listLine.setReply(reply);
            addListViewLineToLv(listLine);
        }
    }

    public void addListViewLineToLv(HospitalListLine hll) {
        Platform.runLater(() -> {
            Iterator<HospitalListLine> iterator = lvRequestReply.getItems().iterator();
            while (iterator.hasNext()) {
                HospitalListLine temp = iterator.next();
                if (temp.getRequest().equals(hll.getRequest())) {
                    iterator.remove();
                    break;
                }
            }
            lvRequestReply.getItems().add(hll);
        });
    }
}
