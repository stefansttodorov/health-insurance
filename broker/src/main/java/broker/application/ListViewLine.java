package broker.application;


import broker.model.TreatmentCostsReply;
import broker.model.TreatmentCostsRequest;

/**
 * This class is an item/line for a ListView. It makes it possible to put both HospitalCostsRequest and HospitalCostsReply object in one item in a ListView.
 */
class ListViewLine {

    private TreatmentCostsRequest treatmentCostsRequest;
    private TreatmentCostsReply treatmentCostsReply;

    public ListViewLine(TreatmentCostsRequest treatmentCostsRequest) {
        setTreatmentRequest(treatmentCostsRequest);
        setTreatmentReply(null);
    }

    public TreatmentCostsRequest getTreatmentRequest() {
        return treatmentCostsRequest;
    }

    private void setTreatmentRequest(TreatmentCostsRequest treatmentCostsRequest) {
        this.treatmentCostsRequest = treatmentCostsRequest;
    }

    public TreatmentCostsReply getTreatmentReply() {
        return treatmentCostsReply;
    }

    public void setTreatmentReply(TreatmentCostsReply treatmentCostsReply) {
        this.treatmentCostsReply = treatmentCostsReply;
    }

    /**
     * This method defines how one line is shown in the ListView.
     * @return
     *  a) if HospitalReply is null, then this item will be shown as "treatmentCostsRequest.toString ---> waiting for treatment reply..."
     *  b) if HospitalReply is not null, then this item will be shown as "treatmentCostsRequest.toString ---> treatmentCostsReply.toString"
     */
    @Override
    public String toString() {
        return treatmentCostsRequest.toString() + "  --->  " + ((treatmentCostsReply !=null)? treatmentCostsReply.toString():"waiting for treatment reply...");
    }

}
