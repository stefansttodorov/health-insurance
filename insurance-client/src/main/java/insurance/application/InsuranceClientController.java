package insurance.application;

import insurance.gateway.ProducerChainedGateway;
import insurance.model.TreatmentCostsReply;
import insurance.model.TreatmentCostsRequest;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import javax.jms.JMSException;
import java.net.URL;
import java.util.ResourceBundle;

public class InsuranceClientController implements Initializable {
    @FXML
    private ListView<ClientListLine> lvRequestsReplies;
    @FXML
    private TextField tfSsn;
    @FXML
    private TextField tfAge;
    @FXML
    private TextField tfTreatmentCode;
    @FXML
    private CheckBox cbTransport;
    @FXML
    private TextField tfKilometers;

    private ProducerChainedGateway producerChainedGateway;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tfSsn.setText("123456");
        tfAge.setText("56");
        tfTreatmentCode.setText("ORT125");
        cbTransport.setSelected(false);
        tfKilometers.setDisable(true);

        this.producerChainedGateway = new ProducerChainedGateway() {
            public void onLoanReplyArrived(TreatmentCostsRequest treatmentCostsRequest, TreatmentCostsReply treatmentCostsReply){
                // Get Line for Loan Request
                ClientListLine clientListLine = getLvlForLoanRequest(treatmentCostsRequest);
                // Set LoanReply to Line
                clientListLine.setReply(treatmentCostsReply);
                // Update Line on ListView
                addLineToView(clientListLine);
            }
        };
    }

    private ClientListLine getListLine(TreatmentCostsRequest request) {
        for (ClientListLine clientListLine : lvRequestsReplies.getItems()){
            if (clientListLine.getRequest() == request) {
                return clientListLine;
            }
        }
        return null;
    }

    public void transportChanged(){
        System.out.println(cbTransport.isSelected());
        if (!cbTransport.isSelected()){
            tfKilometers.setText("");
        }
        this.tfKilometers.setDisable(!this.cbTransport.isSelected());
    }

    public void btnSendClicked(){
        try {
            int ssn = Integer.parseInt(this.tfSsn.getText());
            String treatmentCode = this.tfTreatmentCode.getText();
            int age = Integer.parseInt(this.tfAge.getText());

            int transportDistance = 0;
            if (this.cbTransport.isSelected()) {
                transportDistance = Integer.parseInt(this.tfKilometers.getText());
            }
            // Create a Treatment Request out of user input
            TreatmentCostsRequest treatmentCostsRequest = new TreatmentCostsRequest(
                    ssn,
                    age,
                    treatmentCode,
                    transportDistance
            );

            // Convert request to ClientListLine
            ClientListLine clientListLine = new ClientListLine(treatmentCostsRequest, null);

            // Add ClientListLine to List View
            addLineToView(clientListLine);

            // Send Treatment Request to Broker
            this.producerChainedGateway.applyForLoan(treatmentCostsRequest);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    private void addLineToView(ClientListLine clientListLine) {
        Platform.runLater(() -> {
            this.lvRequestsReplies.getItems().remove(clientListLine);
            this.lvRequestsReplies.getItems().add(clientListLine);
        });
    }

    private ClientListLine getLvlForLoanRequest(TreatmentCostsRequest treatmentCostsRequest) {

        for(ClientListLine clientListLine : this.lvRequestsReplies.getItems()) {
            if(treatmentCostsRequest.equals(clientListLine.getRequest())) {
                return clientListLine;
            }
        }
        return null;
    }
}
