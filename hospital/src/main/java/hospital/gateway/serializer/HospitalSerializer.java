package hospital.gateway.serializer;

import hospital.model.HospitalCostsReply;
import hospital.model.HospitalCostsRequest;
import com.google.gson.Gson;

public class HospitalSerializer {
    private Gson gson;

    public HospitalSerializer() {
        this.gson = new Gson();
    }

    // Hospital Request TO JSON
    public String hospitalRequestToJson(HospitalCostsRequest approvalRequest){
        return gson.toJson(approvalRequest);
    }

    // Hospital Request FROM JSON
    public HospitalCostsRequest hospitalRequestFromJson(String replyString){
        return gson.fromJson(replyString, HospitalCostsRequest.class);
    }

    // Hospital Reply TO JSON
    public String hospitalReplyToJson(HospitalCostsReply approvalReply){
        return gson.toJson(approvalReply);
    }

    // Hospital Reply FROM JSON
    public HospitalCostsReply hospitalReplyFromJson(String replyString){
        return gson.fromJson(replyString, HospitalCostsReply.class);
    }

}


