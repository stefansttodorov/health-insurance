package hospital.gateway;

import hospital.gateway.message.Receiver;
import hospital.gateway.message.Sender;
import hospital.gateway.serializer.HospitalSerializer;
import hospital.model.HospitalCostsReply;
import hospital.model.HospitalCostsRequest;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.HashMap;
import java.util.Map;

public class ChainedGateway {
    // Declare Brokers
    private Receiver bankReceiver;
    private Sender bankSender;

    // Declare Message Queue Names
    private String receiverQueueName;
    private String senderQueueName;

    // Declare Serializers
    private HospitalSerializer hospitalSerializer;

    // Declare HashMap
    private Map<HospitalCostsRequest, String> map;
    private Map<String, Integer> correlationToAggregationMap;

    public ChainedGateway(String receiveQueue, String senderQueue) {
        // Initialize Hash Map
        map = new HashMap<>();
        correlationToAggregationMap = new HashMap<>();

        // Initialize Queue Names
        this.receiverQueueName = receiveQueue;
        this.senderQueueName = senderQueue;

        // Initialize Serializer
        this.hospitalSerializer = new HospitalSerializer();

        // Initialize Bank Brokers
        this.bankReceiver = new Receiver(receiveQueue);
        this.bankSender = new Sender(senderQueue, receiveQueue);

        this.bankReceiver.setMessageListener(message -> {
            try {
                // Convert Message to String
                String messageJson = ((TextMessage) message).getText();
                // Deserialize Message
                HospitalCostsRequest messageReceived = hospitalSerializer.hospitalRequestFromJson(messageJson);
                // Store BankInterestRequest in Map
                this.map.put(messageReceived, message.getJMSMessageID());
                this.correlationToAggregationMap.put(message.getJMSMessageID(), message.getIntProperty("aggregationID"));
                // Send to Controller
                onBrokerMessageReceived(messageReceived);
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });

    }

    public void processCosts(HospitalCostsRequest hospitalCostsRequest, HospitalCostsReply hospitalCostsReply)
            throws JMSException {
        // Deserialize and create a message to be sent
        Message messageToSend = bankSender.createTextMessage(hospitalSerializer.hospitalReplyToJson(hospitalCostsReply));
        String correlationId = this.map.get(hospitalCostsRequest);
        // Retrieve BankInterestRequest from Map and set CorrelationID to Message
        messageToSend.setJMSCorrelationID(correlationId);
        // get necessary information from maps
        Integer aggregationId = this.correlationToAggregationMap.get(correlationId);
        messageToSend.setIntProperty("aggregationID", aggregationId);
        // Send message
        this.bankSender.sendMessage(messageToSend);

    }

    public void onBrokerMessageReceived(HospitalCostsRequest hospitalCostsRequest) {}
}
