package hospital.gateway.message;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class Sender {
    private Connection connection;
    private Session session;

    private MessageProducer messageProducer;
    private Properties properties;
    private String destinationQueue;
    private String receivingQueue;
    private Destination destinationToSendTo;

    public Sender(String destinationQueue, String receivingQueue){
        // Set Queue names
        this.destinationQueue = destinationQueue;
        this.receivingQueue = receivingQueue;

        // Set and Apply Properties
        properties = new Properties();
        properties.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
        properties.setProperty(Context.PROVIDER_URL, "tcp://localhost:61616");
        this.properties.put(("queue." + destinationQueue), destinationQueue);

        try {
            // Create Session
            Context jndiContext = new InitialContext(properties);
            ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("ConnectionFactory");
            this.connection = connectionFactory.createConnection();
            this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            // Connect to the sender destination
            this.destinationToSendTo = (Destination) jndiContext.lookup(destinationQueue);
            this.messageProducer = session.createProducer(destinationToSendTo);

        } catch (NamingException | JMSException e){
            e.printStackTrace();
        }
    }

    public Message createTextMessage(String messageBody) throws JMSException {
        return this.session.createTextMessage(messageBody);
    }

    public void sendMessage(Message message) throws JMSException {
        // Set Reply Destination
        message.setJMSReplyTo(session.createQueue(receivingQueue));
        // Send Message
        messageProducer.send(message);
    }
}
