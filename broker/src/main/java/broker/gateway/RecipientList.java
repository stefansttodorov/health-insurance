package broker.gateway;

import broker.gateway.hospital.ChainedGateway;
import broker.model.HospitalCostsRequest;
import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;

import javax.jms.JMSException;

public class RecipientList {

    // Declare and store all hospital receiving endpoints
    private String catharinaRequestQueue = "catharinaRequestQueue";
    private String maximaRequestQueue = "maximaRequestQueue";
    private String umcRequestQueue = "umcRequestQueue";

    // Define Jeval rules for all hospitals
    private String catarinaJevalRule ="startsWith('#{treatmentCode}', 'ORT', 0) && 10 <= #{patientAge}";
    private String maximaJevalRule = "18 <= #{patientAge}";

    // Declare the chained gateway
    private ChainedGateway hospitalChainedGateway;

    // Define the constructor
    public RecipientList(ChainedGateway hospitalChainedGateway) {
        this.hospitalChainedGateway = hospitalChainedGateway;
    }

    // Send HospitalCostsRequest to hospitals based on the defined rules
    public int sendHospitalCostsRequest(HospitalCostsRequest hospitalCostsRequest, Integer aggregationId) throws JMSException {
        // Define and Initialize the Evaluator
        Evaluator evaluator = createEvaluator(hospitalCostsRequest);

        // Set counter to keep track of how many hospitals receive the request
        // Counter set to 1 since UMC hospital has no rules
        int counter = 1;

        try {
            // Evaluate Catharina rule, send request and increment counter
            if (("1.0").equals(evaluator.evaluate(catarinaJevalRule))) {
                this.hospitalChainedGateway.requestCosts(
                        hospitalCostsRequest,
                        aggregationId,
                        catharinaRequestQueue
                );
                counter++;
            }
            // Evaluate Maxima rule, send request and increment counter
            if (("1.0").equals(evaluator.evaluate(maximaJevalRule))) {
                this.hospitalChainedGateway.requestCosts(
                        hospitalCostsRequest,
                        aggregationId,
                        maximaRequestQueue
                );
                counter++;
            }
        } catch (EvaluationException e) {
            e.printStackTrace();
        }

        // Always send request to UMC
        this.hospitalChainedGateway.requestCosts(
                hospitalCostsRequest,
                aggregationId,
                umcRequestQueue
        );

        // Return counter
        return counter;
    }


    // Helper method that initializes Jeval Evaluator and processes the request
    private Evaluator createEvaluator(HospitalCostsRequest hospitalCostsRequest) {
        // Define and Initialize the Evaluator
        Evaluator evaluator = new Evaluator();

        // Process the request
        evaluator.putVariable("treatmentCode", hospitalCostsRequest.getTreatmentCode());
        evaluator.putVariable("patientAge", Integer.toString(hospitalCostsRequest.getAge()));

        // Return the results
        return evaluator;
    }
}